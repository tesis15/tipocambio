/** DECLARACION DE MODULOS QUE VAMOS A EMPLEAR PARA ESTE CASO USAREMOS REQUEST PARA CONSUMIR EL SERVICIO DE BANXICO
 *  EXPRESS COMO SERVIDOR PARA EXPONER EL SERVICIO Y MARIADB PARA CONECTARNOS A LA BASE DE DATOS
 */

const request = require("request");
const express = require("express");
const mariadb = require('mariadb');
const app=express();

/** TOKEN ES UN VALOR PROPORCIONADO EN BANXICO QUE NOS PERMITE HACER SOLICITUDES PARA OBTENERLO ES NECESARIO ENTRAR
 * A LA PAGINA DE BANXICO https://www.banxico.org.mx/SieAPIRest/service/v1/token
 * SE DECLARAN DOS VARIABLES QUE ES EL PUERTO POR EL CUAL CORRERA NUESTRO SERVICO Y QUE ACEPTARA LAS PETICIONES
 * DESDE CUALQUIER IP
 */

const  token='e5c4cee51f6a8799acf6c074b48e16b80ea1e6df657f1f237fec518a3d8e79ae';
const series='SF60653,SF46410,SF46407'
var port=process.env.SERVERPORT || 8080
var serverip=process.env.SERVERIP || '0.0.0.0'

/** CREAMOS UNA CONSTANTE QUE NOS SERVIRA PARA LA CONEXION A LA BASE DE DATOS ASI COMO LA EJECUCION DE QUERIES */
const pool = mariadb.createPool({
    host: process.env.DATABASE_URL || '192.168.0.27',
    user: process.env.DATABASE_USER || 'tesis',
    password: process.env.DATABASE_PASSWORD || 'serviciotesis',
    database: process.env.DATABASE_NAME || 'SERVICIOS',
    connectionLimit: 5
});


/** DECLARAMOS EL SERVICIO QUE VAMOS A EXPONER QUE ES UN GET Y LA URI DEL SERVICIO ASI COMO QUE VA A LEER EL VALOR DE LA 
 * DEL TIPO DE MONEDA A CONSULTAR Y HACER UN CAMBIO DE EL NOMBRE POR UNA SERIE YA QUE BANXICO MANEGA UN CATALOGO DE 
 * SERIES QUE VA DE ACUERDO AL TIPO DE MONEDA
 *  https://www.banxico.org.mx/SieAPIRest/service/v1/doc/catalogoSeries
*/
app.get('/tipocambio', function(req, res) {

/** CONSUMIMOS EL SERVICIO EXPUESTO POR BANXICO DE ACUERDO A LAS ESPECIFICACIONES EN SU PAGINA 
 * https://www.banxico.org.mx/SieAPIRest/service/v1/doc/ejemplos
 * Y ALMACENAMOS LOS DATOS EN NUESTRA BASE DE DATOS 
 */


  request.get("https://www.banxico.org.mx/SieAPIRest/service/v1/series/"+series+"/datos/oportuno?token="+token+"", async function (error, response, body)  {
        if(error) {
            return console.log(error);
        }

        var obj;
        let conn;
        let row;
        let divisa;

        obj=JSON.parse(response.body);
        const series=obj.bmx.series;
       
        try {

        for (const i in series) {
            const serie=series[i];
            const idserie=series[i].idSerie;
            const fecha=serie.datos[0].fecha;
            const valor=serie.datos[0].dato;

            
           
                if(idserie === 'SF46410'){
                    divisa='Euro'
                }
               
                if(idserie === 'SF60653'){
                   divisa='Dolar'
                }    
               
                if(idserie === 'SF46407'){
                   divisa='Libra'
                }     
                conn = await pool.getConnection();
                row= await conn.query(`INSERT INTO TIPO_CAMBIO (FECHA, MONEDA, TIPO_CAMBIO) VALUES (sysdate(),'${divisa}',${parseFloat(valor).toFixed(2)})`);
        
            }}
            catch (error) {
                 conn.release();
                 conn.destroy();
                 console.log(error);

            }
            conn.release(); 
            conn.destroy();
            res.json(row);
           
               

    });

});


/** EXPONEMOS EL SERVIDOR DE EXPRESS  */

app.listen(port, serverip, function(req, res) {
   console.log("Servicios de tipo de cambio se encuentra arriba");
});



